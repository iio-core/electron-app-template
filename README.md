# vt-desktop

## Project setup

```bash
npm install
```

### Compiles and hot-reloads for development  

```bash
npm run electron:serve
```

### Compiles and minifies for production  

```bash
npm run electron:build
```

### Lints and fixes files  

```bash
npm run lint
```

## Working with HTTP gateway

### Preparing for development

You need first to prepare your environment for gateway deployment (dev mode):

```bash
tools/helper/prepare-dependencies.sh
```

This will create a _docker-compose.yml_ file, as well as _dev\_start.sh_ one that
allows you to start desktop app in parallel with HTTP gateway. 

### Build datum and dlake

In order to provide necessary info for building corresponding _dlake_ service,
you need first to build compliant datum files, starting from local datum files:

```bash
tools/build-datum.js
```

Then, you can build _dlake_ service's image:

```bash
npm run build:dlake:image
```

and/or eventually publish it for production use:

```bash
npm run publish:dlake:docker
```

### Start desktop and HTTP gateway (dev mode)

```bash
./dev_start.sh
```
