import * as d3 from 'd3'

import './app.css'
import 'typeface-roboto'
import 'material-design-icons/iconfont/material-icons.css'

import Vue from 'vue'

import App from './components/App.vue'
import {getRouter} from '@ignitial/iio-electron-app'
import {getStore} from '@ignitial/iio-electron-app'
import {sync} from 'vuex-router-sync'

// Vue plugins
import {configPlugin} from '@ignitial/iio-electron-app'
import {wsPlugin} from '@ignitial/iio-electron-app'
import {authPlugin} from '@ignitial/iio-electron-app'
import {servicesPlugin} from '@ignitial/iio-electron-app'
import {modulesPlugin} from '@ignitial/iio-electron-app'
import {i18nPlugin} from '@ignitial/iio-electron-app'
import {utilsPlugin} from '@ignitial/iio-electron-app'
import {syncdbPlugin} from '@ignitial/iio-electron-app'
import {geoPlugin} from '@ignitial/iio-electron-app'

// Vuetify
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

// recursive components
import {JSONFormItem} from '@ignitial/iio-electron-app'
import {JSONViewer} from '@ignitial/iio-electron-app'
// other components
import {JSONForm} from '@ignitial/iio-electron-app'
import {FileDrop} from '@ignitial/iio-electron-app'
import {ColorPicker} from '@ignitial/iio-electron-app'
import {Geo} from '@ignitial/iio-electron-app'

// files
import fs from 'fs'
import path from 'path'

// populate
import populate from '../tools/populate_db-local'

// -----------------------------------------------------------------------------
// Specific imports
import Dashboard from './views/Dashboard.vue'
// -----------------------------------------------------------------------------

// Vue configuration
Vue.config.productionTip = false

// get instances
let router = getRouter(Vue)
let store = getStore(Vue)

// -----------------------------------------------------------------------------
// Specific routes
router.addRoutes([
  {
    path: '/',
    name: 'rootPath',
    component: Dashboard,
    beforeEnter: (to, from, next) => {
      let token = localStorage.getItem('token')
      if (token && token !== 'null') {
        next()
      } else {
        next({ path: '/login' })
      }
    }
  }
])
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Specific store
//
/*
store.registerModule('myNamespace', {
  state: {
    myState: null
  },
  mutations: {
    myState(state, value) {
      state.myState = value
    }
  }
})
*/
// -----------------------------------------------------------------------------

// router sync
sync(store, router)

// Vue plugins use
Vue.use(Vuetify)
Vue.use(configPlugin)
Vue.use(wsPlugin)
Vue.use(utilsPlugin)
Vue.use(authPlugin)
Vue.use(servicesPlugin)
Vue.use(modulesPlugin)
Vue.use(i18nPlugin)
Vue.use(syncdbPlugin)
Vue.use(geoPlugin)

// -----------------------------------------------------------------------------
// Specific plugins
// ...
// Vue.use(cachePlugin)
// ...
// -----------------------------------------------------------------------------

// create app
const app = new Vue({
  router,
  store,
  ...App
})

// -----------------------------------------------------------------------------
// inititiliaze syncdb plugin/service
//
// Each collection has to be added as an item in that collections array that
// is passed as parameter to initialize method.
// Each item is an object with two keys (name, klass) and their corresponding
// values:
//
// app.$syncdb.initialize(populate, [ { name: 'mydatumname', klass: MyDatumClass }, ... ])
//
app.$syncdb.initialize(populate)
// -----------------------------------------------------------------------------

// recursive components
Vue.component('jsonform-item', JSONFormItem)
Vue.component('jsonviewer', JSONViewer)
// other components
Vue.component('jsonform', JSONForm)
Vue.component('file-drop', FileDrop)
Vue.component('colorpicker', ColorPicker)
Vue.component('geo', Geo)

// -----------------------------------------------------------------------------
// Specific components
// ...
// Vue.component('mycomponent', MyComponent)
// ...
// -----------------------------------------------------------------------------

// manage splashscreen/progress
d3.select('#splashscreen').style('opacity', 0)
d3.select('#progress').style('opacity', 0)

setTimeout(() => {
  d3.select('#splashscreen').remove()
  d3.select('#progress').remove()
}, 1000)

export {app, router, store}

let uploadsFolder = path.join(process.cwd(), 'uploads')

if (!fs.existsSync(uploadsFolder)) {
  fs.mkdirSync(uploadsFolder)
  console.log('uploads created', uploadsFolder)
} else { 
  console.log('uploads already created')
}

// -----------------------------------------------------------------------------
// Specifics
