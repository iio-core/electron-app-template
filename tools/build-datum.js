#!/usr/bin/env node
const _ = require('lodash')
const fs = require('fs')
const path = require('path')
const roles = require(path.join(__dirname, 'data/roles'))

let datumPath = path.join(__dirname, '../src/datum')
let outputPath = path.join(__dirname, '../datum')
let template = 
  fs.readFileSync(path.join(__dirname, 'templates/template.datum.js'), 'utf8')

let files = fs.readdirSync(datumPath)
let collections = []

console.log('Finding datum classes...')
for (let f of files) {
  let content = fs.readFileSync(path.join(datumPath, f), 'utf8')
  let kName = content.match(/class (.*?) /)[1]
  let cName = content.match(/super\(syncdb, \'(.*?)\'\)/)[1]
  let output = '' + template
  output = output.replace(/_DATUM_/g, kName).replace(/_COLLECTION_/g, cName)
  fs.writeFileSync(path.join(outputPath, f), output, 'utf8')
  collections.push(cName)
}

console.log('Updating config...')
let configPath =  path.join(__dirname, '../config/config.json')
let config = JSON.parse(fs.readFileSync(configPath, 'utf8'))
config.offline.collections = _.union(config.offline.collections, collections)
fs.writeFileSync(configPath, JSON.stringify(config, null, 2), 'utf8')

console.log('Updating roles...')
for (let collection of collections) {
  roles.admin[collection] = {
    'create:any': [ '*' ],
    'read:any': [ '*' ],
    'update:any': [ '*' ],
    'delete:any': [ '*' ]
  }

  roles.user[collection] = {
    'create:any': [ '*' ],
    'read:any': [ '*' ],
    'update:any': [ '*' ],
    'delete:any': [ '*' ]
  }
}

let content = JSON.stringify(roles, null, 2)
fs.writeFileSync(path.join(__dirname, 'data/roles.json'), content, 'utf8')

console.log('done')