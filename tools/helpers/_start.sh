#!/bin/sh

echo "Prepare docker-compose.yml..."

# sets app version
APP_VERSION=$(cat ./package.json \
  | grep version \
  | head -1 \
  | awk -F: '{ print $2 }' \
  | sed 's/[",]//g' \
  | tr -d '[[:space:]]')

cat tools/helpers/template.docker-compose.yml | sed "s/_user_/$USER/g" | sed "s/APP_VERSION/$APP_VERSION/g" > docker-compose.yml

docker-compose up -d
sleep 1

npm run electron:serve
