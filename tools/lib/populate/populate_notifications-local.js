import sublevel from 'subleveldown'

import {deleteAll} from './utils'

/*
let status = [
  "noack",
  "ack"
]

let levels = [
  "spam",
  "info",
  "warn",
  "alert",
  "critical"
]

let template = {
  "message": "Changement d'équipe au centre d'information passagers",
  "user": "gcrood",
  "link": "",
  "level": "info",
  "status": "noack",
  "image": "assets/"
}
*/

export let populate = async (db, resetOnly) => {
  console.log('updating notifications...')

  let notifications = sublevel(db, 'notifications', { valueEncoding: 'json' }) 

  try {
    await deleteAll(notifications) // reset
    console.log('notifications reset')
  } catch (err) {
    console.log(err)
  }

  if (resetOnly) return

  console.log('notifications done.')
}
