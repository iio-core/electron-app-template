import sublevel from 'subleveldown'
import fs from 'fs'
import path from 'path'

const rolesData = JSON.parse(
  fs.readFileSync(path.join(process.cwd(), 'tools/data/roles.json'), 'utf8'))

const ObjectID = require('bson-objectid')

import {deleteAll} from './utils'

export let populate = async (db, resetOnly) => {
  console.log('updating roles...')
  let roles = sublevel(db, 'roles', { valueEncoding: 'json' })
  try {
    await deleteAll(roles)
    console.log('roles reset')
  } catch (err) {
    console.log(err)
  }

  if (resetOnly) return

  try {
    rolesData._id = new ObjectID()
    await roles.put(rolesData._id, rolesData)
  } catch (err) {
    console.log(err)
  }

  console.log('roles done.')
}
