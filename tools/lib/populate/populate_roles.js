const fs = require('fs')
const path = require('path')

exports.populate = async (db, resetOnly) => {
  const rolesData = JSON.parse(
    fs.readFileSync(path.join(__dirname, '../../data/roles.json'), 'utf8'))
    
  console.log('updating roles...')
  let roles = db.collection('roles')

  try {
    await roles.deleteMany({}) // reset
    console.log('roles reset')
  } catch (err) {
    console.log(err)
  }

  if (resetOnly) return

  try {
    await roles.insertOne(rolesData, { w: 1 })
  } catch (err) {
    console.log(err)
  }

  console.log('...roles done')
}
