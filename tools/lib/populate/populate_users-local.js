import sublevel from 'subleveldown'

const bcrypt = require('bcryptjs')
const _ = require('lodash')
const ObjectID = require('bson-objectid')

import {deleteAll} from './utils'

let salt = bcrypt.genSaltSync(10)
let hash = bcrypt.hashSync('toto13!', salt)

let additionalUsers = [
  {
    "username": "tcrood",
    "firstname": "Thunk",
    "lastname": "Crood",
    "password": hash,
    "avatar": '',
    "role": 'user',
    "contactInfo": {
      "email": "vdrac@free.fr",
      "phone": {
        "mobile": "",
        "office": ""
      }
    },
    "settings": {
      "notificationsByEmail": false
    },
    _lastModified: new Date()
  }
]

let template = {
  "username": "gcrood",
  "firstname": "Grug",
  "lastname": "Crood",
  "password": hash,
  "avatar": '',
  "role": 'admin',
  "contactInfo": {
    "email": "contact@ignitial.fr",
    "phone": {
      "mobile": "06 66 66 66 66",
      "office": "06 66 66 66 66"
    }
  },
  "settings": {
    "notificationsByEmail": false
  },
  _lastModified: new Date()
}

export let populate = async (db, resetOnly) => {
  console.log('updating users...')
  let users =  sublevel(db, 'users', { valueEncoding: 'json' })

  try {
    await deleteAll(users) // reset
    console.log('users reset')
  } catch (err) {
    console.log(err)
  }

  if (resetOnly) return

  try {
    // insert admin
    let admin = _.cloneDeep(template)
    admin._id = new ObjectID()
    await users.put(admin._id, admin)
  
    for (let user of additionalUsers) {
      user._id = new ObjectID()
      await users.put(user._id, user)
    }
  } catch (err) {
    console.log(err)
  }

  console.log('users done.')
/*
  let readStream = users.createValueStream()
  readStream.on('data', function(e) {
    console.log('>>>>>>>>>>>>>>>>>>', e)
  }).on('error', function(err) {
    console.log(err)
  }).on('close', () => {
    console.log('populate closed')
  })
*/
}
