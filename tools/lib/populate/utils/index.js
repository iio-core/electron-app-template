export function deleteAll(collection) {
  return new Promise((resolve, reject) => {
    collection.createKeyStream()
      .on('data', key => collection.del(key))
      .on('error', err => {
        reject(err)
      })
      .on('end', () => {
        resolve()
      })
  }) 
}

export function find(db, collection, query) {
  return new Promise((resolve, reject) => {
    let subdb = sublevel(db, collection, { valueEncoding: 'json' })
    let docs = []
    let filter
    if (query && JSON.stringify(query) !== '{}') {
      filter = queryBuilder(query)
    }

    let readStream = subdb.createValueStream()

    readStream.on('data',function(e) {
      if (filter && filter(e)) {
        docs.push(e)
      } else if (!filter) {
        docs.push(e)
      }
    }).on('error', function(err) {
      reject(err)
    }).on('close', () => {
      resolve(docs)
    })
  })
}