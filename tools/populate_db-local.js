const users = require('./lib/populate/populate_users-local.js').populate
const roles = require('./lib/populate/populate_roles-local.js').populate
const notifications = require('./lib/populate/populate_notifications-local.js').populate

export default async function run(db) {
  try {
    let resetOnly = false // set only if necessary

    await roles(db, resetOnly)
    await users(db, resetOnly)
    await notifications(db, resetOnly)
    console.log('populate done.')
  } catch (err) {
    console.log('error connecting to db', err)
  }
}