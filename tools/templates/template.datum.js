const Item = require('../lib/core/item').Item

/* */
class _DATUM_ extends Item {
  constructor(options) {
    options.name = '_COLLECTION_'
    super(options)
    this._appendOnly = false
  }
}

exports._DATUM_ = _DATUM_
